
-- SUMMARY --

The account_sync_profile module allows you to synchronize drupal user
pictures across multiple Drupal sites.

This module uses system_retrieve_file() function to get the picture from
the other site, so the user picture should be accesible from the other server.

It is not recommended to use this module in a production environment. It's
still in the early stages of development and testing and you could risk
breaking your accounts or opening up security holes by using it. You should
have complete trust in all sites (and their admins) that you're using this
module with.

The module is not working properly behing the scene, for instance if you are
running a queue to update the users, to make it work th $base_url variable
should be set in the settings.php file with the server URL.

-- DEPENDENCIES --

account_sync_profile requires the Account Sync module:
https://www.drupal.org/project/account_sync

-- INSTALLATION --

 * Install the account_sync module as usual on all sites you wish to use it
   see http://drupal.org/node/70151 for further information.

 * Install the account_sync_profile module as usual on all sites you wish
   to use it.

-- CONTACT --

For bug reports, feature suggestions and latest developments visit the
project page: http://drupal.org/project/account_sync_profile

Maintainers:
 * Luis Nicanor (luisnicg) <luis.nicg@gmail.com>
